from flask_sqlalchemy import SQLAlchemy
from flask_kvsession import KVSessionExtension

db = SQLAlchemy()
kvsession = KVSessionExtension()