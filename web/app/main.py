import os
import datetime
from importlib import import_module
from urllib import parse

from flask import Flask, abort, request, current_app, render_template
from flask_cors import CORS
from subprocess import call, PIPE, Popen
from flask_jwt_extended import JWTManager
from simplekv.memory.redisstore import RedisStore
import redis


from werkzeug.contrib.fixers import ProxyFix
# from urllib import parse
# from importlib import import_module
from app.api import api_v1_bp
from app.cern_oauth import load_cern_oauth
from app.extensions import db, kvsession
from app.config import BaseConfig

app = Flask(__name__)

"""
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration.
    :return: The created application
    """


# app = Flask(__name__)
app.config.from_object(BaseConfig)

CORS(app)
JWTManager(app)

if app.config.get('USE_PROXY', True):
    app.wsgi_app = ProxyFix(app.wsgi_app)


def _initialize_api_blueprints(application):
    from app.api import auth_bp
    # from app.api.resources.examples import example_bp
    # from app.api.resources.browser_handling import browser_bp
    # from app.api.resources.announcements import announcements_bp

    # API
    application.register_blueprint(auth_bp)

    application.register_blueprint(
        api_v1_bp, url_prefix='/api/v1')
    application.logger.debug(api_v1_bp.name)
    application.logger.debug(api_v1_bp.import_name)

def _set_simplekv_store(application):
    with application.app_context():
        """
        If the performance starts to be shitty, change this to RedisStore
        """
        session_store = RedisStore(redis.StrictRedis(host=application.config['REDIS_HOST'],
                                                     port=application.config['REDIS_PORT'],
                                                     password=application.config['REDIS_PASSWORD']))
        kvsession.init_app(app=application, session_kvstore=session_store)

        # Enable and configure the JWT blacklist / token revoke. We are using
        # an in memory store for this example. In production, you should
        # use something persistent (such as redis, memcached, sqlalchemy).
        # See here for options: http://pythonhosted.org/simplekv/
        application.config['JWT_BLACKLIST_ENABLED'] = True
        application.config['JWT_BLACKLIST_STORE'] = session_store

        # Check all tokens (access and refresh) to see if they have been revoked.
        # You can alternately check only the refresh tokens here, by setting this
        # to 'refresh' instead of 'all'
        application.config['JWT_BLACKLIST_TOKEN_CHECKS'] = 'all'
        application.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(minutes=5)

@app.template_filter('urlencode')
def url_decode(string):
    return parse.unquote(string)


app.jinja_env.filters['url_decode'] = url_decode

"""
Load all the available models
"""
with app.app_context():
    for mod in app.config.get('DB_MODELS_IMPORTS', list()):
        import_module(mod)


@app.before_first_request
def setup_database(*args, **kwargs):
    with app.app_context():
        current_app.logger.debug("Initializing the database...")
        db.create_all()

"""
Enable the CERN Oauth Authentication
"""
load_cern_oauth(app)

_initialize_api_blueprints(app)

_set_simplekv_store(app)

#
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)


def host_name():
    return Popen("hostname", stdout=PIPE).stdout.read()


@app.route("/")
def hello():
    # return "Hello World"
    app_name = os.getenv('APP_NAME', "Flask")

    app_desc = os.getenv('APP_DESC',
                         "Flask is a microframework for Python based on Werkzeug, Jinja 2 and good intentions")

    current_app.logger.debug("Loading index")

    return render_template('index.html')



# @app.route("/msg", methods=["POST", "GET"])
# def msg():
#   print("Args %s, %s", (request.args.get('name'), request.args.items))
#   return "MSG Received: %s. <br/> ContainerID: %s" % (request.data if request.data else request.query_string, host_name())

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=8080, ssl_context='adhoc')
