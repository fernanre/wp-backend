from flask_login import UserMixin
from flask_dance.consumer.backend.sqla import OAuthConsumerMixin
from app.extensions import db


class User(db.Model, UserMixin):
    """
    Represents an application User
    """
    __tablename__ = 'user'
    __table_args__ = (db.CheckConstraint('email = lower(email)', 'lowercase_email'),)

    id = db.Column(db.Integer, primary_key=True)
    personid = db.Column(db.Integer, nullable=False, unique=True, index=True)
    firstname = db.Column(db.String(255), nullable=False)
    username = db.Column(db.String(255), nullable=False)
    lastname = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)

    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())

    @property
    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __repr__(self):
        admin = ', is_admin=True' if self.is_admin else ''
        return '<User({}, {}, personid={}{}): {}>'.format(self.id, self.email, self.personid, admin, self.name)

    def to_json(self):
        return {'id': self.id,
                'firstname': self.firstname,
                'lastname': self.lastname,
                'email': self.email,
                'is_admin': self.is_admin}


class OAuth(db.Model, OAuthConsumerMixin):
    """
    Represents an Oauth connection in the application
    """
    __tablename__ = 'flask_dance_oauth'

    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)
