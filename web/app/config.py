import os

from app.utils import str2bool


class RedisConfig(object):
    """
    CERN Oauth configuration
    """
    REDIS_HOST = os.environ['REDIS_HOST']
    REDIS_PORT = os.environ['REDIS_PORT']
    REDIS_PASSWORD = os.environ['REDIS_PASSWORD']


class OauthConfig(object):
    """
    CERN Oauth configuration
    """
    CERN_OAUTH_CLIENT_ID = os.environ['CERN_OAUTH_CLIENT_ID']
    CERN_OAUTH_CLIENT_SECRET = os.environ['CERN_OAUTH_CLIENT_SECRET']
    CERN_OAUTH_AUTHORIZE_URL = os.environ['CERN_OAUTH_AUTHORIZE_URL']
    CERN_OAUTH_TOKEN_URL = os.environ['CERN_OAUTH_TOKEN_URL']


class ApiConfig(object):
    API_OAUTH_CLIENT_ID = os.environ['API_OAUTH_CLIENT_ID']
    API_OAUTH_CLIENT_SECRET = os.environ['API_OAUTH_CLIENT_SECRET']
    API_OAUTH_REDIRECT_URL = os.environ['API_OAUTH_REDIRECT_URL']
    TOKEN_EXPIRATION_SECONDS = int(os.environ['TOKEN_EXPIRATION_SECONDS'])


class DatabaseConfig(object):
    """
    Application database configuration
    """
    DB_NAME = os.environ['DB_NAME']
    DB_PASS = os.environ['DB_PASS']
    DB_PORT = os.environ['DB_PORT']
    DB_SERVICE = os.environ['DB_SERVICE']
    DB_USER = os.environ['DB_USER']

    """
    Database URI will be generated with the already gotten database parameters
    """
    SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(
        DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
    )


class BaseConfig(OauthConfig, ApiConfig, DatabaseConfig, RedisConfig):

    SECRET_KEY = os.environ['SECRET_KEY']
    USE_PROXY = str2bool(os.environ['USE_PROXY'])

    """
    Stores the app configuration
    """
    DB_MODELS_IMPORTS = ('app.models',)
