import base64
import os
import codecs
from functools import wraps

import datetime

import requests
from flask import make_response
from flask import request

from flask_restful import reqparse
from flask_restful import Resource, Api

import json
from flask import Blueprint, abort, current_app, jsonify

from itsdangerous import TimedSerializer, BadSignature
from httplib2 import Http
from oauth2client.client import OAuth2WebServerFlow, FlowExchangeError
from flask_jwt_extended import create_access_token, get_jwt_identity, create_refresh_token, jwt_refresh_token_required, \
    get_raw_jwt, revoke_token
from sqlalchemy.orm.exc import NoResultFound
from marshmallow_jsonapi import fields
from marshmallow_jsonapi.flask import Schema
from flask_rest_jsonapi import ResourceDetail
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_rest_jsonapi.querystring import QueryStringManager as QSManager
from flask_rest_jsonapi.schema import compute_schema

from flask_rest_jsonapi import Api as JsonAPI

from app.extensions import db
from app.models import User, OAuth

auth_bp = Blueprint('auth_api', __name__)
api = Api(auth_bp)


api_v1_bp = Blueprint('api_v1', __name__)
api_v1 = JsonAPI(blueprint=api_v1_bp)


def _get_token_serializer():
    return TimedSerializer(current_app.config['SECRET_KEY'])


def _get_oauth2_identity(auth_code):
    client_id = current_app.config.get('API_OAUTH_CLIENT_ID')
    client_secret = current_app.config.get('API_OAUTH_CLIENT_SECRET')
    redirect_url = current_app.config.get('API_OAUTH_REDIRECT_URL')
    token_url = current_app.config.get('CERN_OAUTH_TOKEN_URL')
    auth_url = current_app.config.get('CERN_OAUTH_AUTHORIZE_URL')

    current_app.logger.debug("Getting Oauth2 user info...")

    info = ""
    if not client_id:
        # Error - No OAuth2 client id configured
        return

    if not client_secret:
        # Error - No OAuth2 client secret configured
        return

    flow = OAuth2WebServerFlow(
        client_id=client_id,
        client_secret=client_secret,
        auth_uri=auth_url,
        token_uri=token_url,
        scope='bio',
        redirect_uri=redirect_url)


    current_app.logger.debug("Oauth2 flow created: {}".format(flow))
    credentials = None
    info = None
    try:
        credentials = flow.step2_exchange(auth_code)

        current_app.logger.debug("Credentials retrieved: {}".format(credentials))

        info = _get_user_info(credentials)

        current_app.logger.debug("User info is: {}".format(info))

    except FlowExchangeError as e:
        current_app.logger.error(e)
        current_app.logger.debug("Client ID: {}".format(client_id))
        current_app.logger.debug("Client Secret: {}".format(client_secret))
        current_app.logger.debug("Client Redirect URL: {}".format(redirect_url))
        return info

    info['tone_token'] = credentials.access_token

    return info


def _get_user_info(credentials):
    current_app.logger.debug("Creating a http request to the API with credentials: {}".format(credentials))

    try:
        http_client = Http()
        if credentials.access_token_expired:
            credentials.refresh(http_client)
        credentials.authorize(http_client)
    except Exception as e:
        current_app.logger.debug(e)

    response, response_body = http_client.request(os.environ['CERN_OAUTH_API_ME'], "GET")
    str_content = response_body.decode('utf8')
    json_content = json.loads(str_content)

    current_app.logger.debug(json_content)
    return json_content


class AuthApi(Resource):
    """
        Class that manages the browser handling API
    """

    def __init__(self):
        """
            Initializes the class instance.

        :return: The class instance
        """

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('authorizationCode', type=str, required=True,
                                   help='No authorizationCode provided', location='json')

        super(AuthApi, self).__init__()

    def post(self):
        """

        Tries to make a request to the CERN Oauth2 API Url and retrieve some user information with the given user token.
        If the token is not valid means that the user is not valid and will be rejected.

        :return: The generated token for the user or an error.
        """
        args = self.reqparse.parse_args()

        auth_code = args['authorizationCode']

        current_app.logger.debug("POST on AuthApi: {}".format(auth_code))

        if auth_code is None:
            abort(401)

        user_info = _get_oauth2_identity(auth_code)
        if not user_info:
            return jsonify({"msg": "Bad credentials"}), 401

        ret1 = {'user_info': user_info}
        current_app.logger.debug(ret1)
        # user_info = json.loads(user_info)

        token = create_access_token(identity=user_info['personid'])

        current_app.logger.debug(token)
        refresh_token = create_refresh_token(identity=user_info['personid'])

        if current_app.config.get('SQLALCHEMY_DATABASE_URI', 'sqlite:///:memory:') != 'sqlite:///:memory:':
            query = User.query.filter_by(personid=user_info['personid'])
            try:
                existing_user = query.one()
            except NoResultFound:
                existing_user = User(username=user_info['username'].strip(),
                                     personid=user_info['personid'],
                                     email=user_info['email'].strip(),
                                     lastname=user_info['last_name'].strip(),
                                     firstname=user_info['first_name'].strip(),
                                     )
                db.session.add(existing_user)
                db.session.commit()

            oauth_model = OAuth()
            oauth_model.created_at = datetime.datetime.now()
            oauth_model.provider = 'cern'
            oauth_model.token = {'tone_token': user_info['tone_token']}
            oauth_model.user = existing_user
            oauth_model.user_id = existing_user.id

            db.session.add(oauth_model)
            existing_user.last_client_login = datetime.datetime.now()
            db.session.commit()

        ret = {'access_token': token,
               'refresh_token': refresh_token,
               'tone_token': user_info['tone_token']
               }

        current_app.logger.debug(ret)

        result = jsonify(ret)

        return make_response(result, 200)


class LogoutApi(Resource):
    """
        Class that manages the browser handling API
    """

    decorators = [jwt_refresh_token_required]

    def post(self):
        """

        Tries to make a request to the CERN Oauth2 API Url and retrieve some user information with the given user token.
        If the token is not valid means that the user is not valid and will be rejected.

        :return: The generated token for the user or an error.
        """
        try:
            self._revoke_current_token()
            current_app.logger.debug("You have logged out")
        except KeyError:
            return make_response(jsonify({
                'msg': 'Access token not found in the blacklist store'
            }), 500)
        return make_response(jsonify({"msg": "Successfully logged out"}), 200)


    # Helper method to revoke the current token used to access
    # a protected endpoint
    def _revoke_current_token(self):
        current_app.logger.debug("Revoking current token")
        current_token = get_raw_jwt()
        jti = current_token['jti']
        revoke_token(jti)

    """
    @module.route("/logout2", methods=['POST'])
    @jwt_refresh_token_required
    def logout():

        try:
            _revoke_current_token()
            current_app.logger.debug("You have logged out")
        except KeyError:
            return jsonify({
                'msg': 'Access token not found in the blacklist store'
            }), 500
        return jsonify({"msg": "Successfully logged out"}), 200
    
    """


class ReauthApi(Resource):
    """
        Class that manages the browser handling API
    """
    decorators = [jwt_refresh_token_required]

    def __init__(self):
        """
            Initializes the class instance.

        :return: The class instance
        """

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('Tone', type=str, required=True,
                                   help='No Tone provided', location='headers')

        super(ReauthApi, self).__init__()

    def post(self):
        """
        
        Tries to make a request to the CERN Oauth2 API Url and retrieve some user information with the given user token.
        If the token is not valid means that the user is not valid and will be rejected.

        :return: The generated token for the user or an error.
        """

        args = self.reqparse.parse_args()
        current_app.logger.debug("Args: {}".format(args['Tone']))

        current_person_id = get_jwt_identity()
        current_app.logger.debug(current_person_id)

        query = User.query.filter_by(personid=current_person_id)
        try:
            existing_user = query.one()

            response = requests.get(os.environ['CERN_OAUTH_API_ME'],
                                    headers={"Authorization": "Bearer " + args["Tone"]});

            current_app.logger.debug("Response: {}".format(response.text))
            current_app.logger.debug("Response Code: {}".format(response.status_code))

            if response.status_code == 200 and response.text:
                try:
                    oauth = OAuth.query.filter(OAuth.token["tone_token"].astext == args["Tone"]).one()

                except NoResultFound:
                    return jsonify({"msg": "Bad credentials"}), 401

                except Exception as e:
                    current_app.logger.error(e)
            else:
                return jsonify({"msg": "Bad credentials"}), 401

        except NoResultFound:
            return jsonify({"msg": "Bad credentials"}), 401

        ret = {
            'access_token': create_access_token(identity=existing_user.personid),
            'tone_token': args["Tone"]
        }

        result = jsonify(ret)

        return make_response(result, 200)


class MeSchema(Schema):
    class Meta:
        type_ = 'users'
        strict = True
        self_view = 'api_v1.user_me'
        self_view_kwargs = {}

    id = fields.Str(dump_only=True)
    personid = fields.Str(dump_only=True)
    firstname = fields.Str(dump_only=True)
    lastname = fields.Str(dump_only=True)
    username = fields.Str(dump_only=True)
    email = fields.Email(dump_only=True)


class Me(ResourceDetail):

    def get(self, *args, **kwargs):
        """Get object details
        """
        self.before_get(args, kwargs)

        current_person_id = get_jwt_identity()

        obj = User.query.filter_by(personid=current_person_id).one()

        qs = QSManager(request.args, self.schema)

        schema = compute_schema(self.schema,
                                getattr(self, 'get_schema_kwargs', dict()),
                                qs,
                                qs.include)

        result = schema.dump(obj).data

        self.after_get(result)
        return result

    schema = MeSchema
    data_layer = {'session': db.session,
                  'model': User
                  }
    decorators = (jwt_required,)


api_v1.route(Me, 'user_me', '/users/me')

api.add_resource(AuthApi, '/api/v1/auth/login/', endpoint='AuthApi')
api.add_resource(ReauthApi, '/api/v1/auth/reauth/', endpoint='ReauthApi')
api.add_resource(LogoutApi, '/api/v1/auth/logout/', endpoint='LogoutApi')



def token_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        try:
            current_app.logger.debug(request.headers)
            try:
                token = base64.b64decode(request.headers['Authorization'])
                token_data = _get_token_serializer().loads(
                    token, max_age=current_app.config['TOKEN_EXPIRATION_SECONDS'])
                current_app.logger.debug(token_data)
            except KeyError:
                abort(401)
        except BadSignature:
            abort(401)
        return f(*args, **kwargs)

    return decorated_function
