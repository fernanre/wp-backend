blinker==1.4
Flask==0.12.2
Flask-Dance==0.10.0
Flask-Login==0.3.2
Flask-SQLAlchemy==2.2
psycopg2==2.6.2
SQLAlchemy-Utils==0.32.14
urllib3==1.21.1
Werkzeug==0.12.2